import rsa


class Encryptor:
    def __init__(self):
        self.public_key, self.private_key = rsa.newkeys(1024)

    def set_key(self, key):
        self.public_key.n = key

    def encrypt(self, message):
        return rsa.encrypt(message.encode(), self.public_key)

    def decrypt(self, message):
        if isinstance(message, str):
            message = message.encode()

        return rsa.decrypt(message, self.private_key)
