import socket
import config
import os
import json
from modules.encryptor import Encryptor
from threading import Thread


class Client:
    def __init__(self, nickname):
        self.encryptor = Encryptor()
        self.nickname = nickname
        self.buffer = ""

    def start(self, ip, port):
        self.socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM
        )

        self.socket.connect((ip, port))

        self.exchange_keys()
        Thread(target=self.start_dialogue).start()
        self.send_messages()

    def exchange_keys(self):
        self.send(
            json.dumps(
                {
                    "type": "exchange_keys",
                    "public_key": self.encryptor.public_key.n,
                    "name": self.nickname
                }
            )
        )

        data = self.receive_data()
        data = json.loads(data)

        if data["type"] == "exchange_keys":
            self.companion_name = data["name"]
            self.encryptor.set_key(data["public_key"])

    def send(self, message):
        if isinstance(message, str):
            message = message.encode()

        self.socket.send(message)

    def encrypt_and_send(self, message):
        message = self.encryptor.encrypt(message)
        self.send(message)

    def start_dialogue(self):
        self.clear_terminal()

        while True:
            message = self.receive_and_decrypt_message()
            if message["type"] == "message":
                self.buffer += \
                    f"< {self.companion_name} > {message['message']}\n"
                self.clear_terminal()
                print(self.buffer)

    def receive_and_decrypt_message(self):
        message = self.receive_data()
        message = self.encryptor.decrypt(message).decode()

        return json.loads(message)

    def receive_data(self):
        message = self.socket.recv(1024)

        try:
            return message.decode()
        except Exception:
            return message

    def send_messages(self):
        while True:
            message = input()

            self.encrypt_and_send(
                json.dumps(
                    {
                        "type": "message",
                        "message": message
                    }
                )
            )

            self.buffer += f"< {self.nickname} > {message}\n"
            self.clear_terminal()
            print(self.buffer)

    @staticmethod
    def clear_terminal():
        os.system("cls" if os.name == "nt" else "clear")


if __name__ == '__main__':
    client = Client(config.NICKNAME)

    ip = input("IP: ")
    port = int(input("PORT: "))

    client.start(ip, port)
